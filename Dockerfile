FROM adoptopenjdk:14-jre-openj9

run apt-get update && apt-get install -y libgtk2.0-0 libnss3 chromium-browser
ARG JAR_FILE
ADD target/${JAR_FILE} /usr/share/myservice/myservice.jar

ENTRYPOINT ["java", "-jar", "/usr/share/myservice/myservice.jar"]