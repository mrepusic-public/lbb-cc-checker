# Amazon Credit Card / Landesbank Berlin Credit Card Cecker (lbb-cc-checker)

## Forword
My credit card provider is a german bank. To see how much money I have spended, I need to log in a web gui. That's just pain. They have a "service" which sends a weekly sms with the current balance for a small fee. Well I tried that I did not receive anything. Support told me to activate the service again... well... no. 

I build this little to to scape the website of the LBB to get my current balance as an json object.

There are some words inside the source code which are german. That's because it's a german bank. Some of these words and documentation might have typos, that's because ... well laziness on my part.

## Goals

I had three goals for this project:

* See how much I have spent with my credit card in the current month.
* Use quarkus - I normaly only work with JAVA EE / Jakarta EE on wildfly. I needed a change.
* Use Selenium with a headless browser (Chromium). This way I could run it in a small docker container on a server far far away.


## Use

The following variables are used to configure the application:

```
LBBCCCHECKER_LBB_USERNAME=[Your username]
LBBCCCHECKER_LBB_PASSWORD=[Your password]
LBBCCCHECKER_LBB_URL_LOGIN=https://amazon.lbb.de/security/login
LBBCCCHECKER_LBB_URL_LOGOUT=https://amazon.lbb.de/api/logout
```

### What you need to know
After use, LLB sends out a E-Mail which notifies you, that a "new device" has logged in your account. Tought for a second, that my account was hacked - but then realized it is the headless chromium :) 

### Development
Start in Quarkus Dev mode: 
```
mvn -DLBBCCCHECKER_LBB_USERNAME= -DLBBCCCHECKER_LBB_PASSWORD= -DLBBCCCHECKER_LBB_URL_LOGIN=https://amazon.lbb.de/security/login -DLBBCCCHECKER_LBB_URL_LOGOUT=https://amazon.lbb.de/api/logout quarkus:dev
```

### Docker
I have defined a maven profile to build the docker container:
```
mvn clean -Pdocker package
```
This will create an image "lbb-cc-checker:1.0-SNAPSHOT"

How I start the docker container:

1. create a file env.properties with the following contents:
 
 ```
LBBCCCHECKER_LBB_USERNAME=
LBBCCCHECKER_LBB_PASSWORD=
LBBCCCHECKER_LBB_URL_LOGIN=https://amazon.lbb.de/security/login
LBBCCCHECKER_LBB_URL_LOGOUT=https://amazon.lbb.de/api/logout
 ```

2. Start the container
 docker run -d --rm --name lbb-cc-checker --env-file=lbb-cc-checker.properties -p 8080:8080 lbb-cc-checker:1.0-SNAPSHOT

### Start Jar file
If you don't like docker, you can use/start the runner jar - you need to have chrome or chromium installed:
```
java -DLBBCCCHECKER_LBB_USERNAME= -DLBBCCCHECKER_LBB_PASSWORD= -DLBBCCCHECKER_LBB_URL_LOGIN=https://amazon.lbb.de/security/login -DLBBCCCHECKER_LBB_URL_LOGOUT=https://amazon.lbb.de/api/logout -jar lbb-cc-checker-runner.jar
```


## Disclaimer:
It's was coded by me for me. I just wanted to learn something new and solve a small problem. I am not responible for any damage resulting from it. I recommend to use it likewise: *_Just look at the code, modify it, use it, learn from it and have fun!_* :)

*I use some open source libraries - respect their licences!*

## Support:
If you like it and want to throw some money in my hat: [https://www.patreon.com/repusic](https://www.patreon.com/repusic)

## TODO
* Basic Authentication. I tron between using quarkus to protect the service and using an nginx/apache in front of it for security... 
* JUnit tests. It hurts me a little to have no tests in the project. Feel free to commit any tests.
* Maybe javadoc... 
* Run sonar on it - but only after I have some tests :)