package com.repusic.lbbccchecker.lbb.cc.checker.boundary;

import com.repusic.lbbccchecker.lbb.cc.lbb.boundary.LLBService;
import com.repusic.lbbccchecker.lbb.cc.lbb.entity.CCStatus;
import java.io.IOException;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 *
 * @author Mijo Repušić
 */
@Path("/")
public class CheckerService {

    @Inject
    private LLBService lLBService;

    private static final System.Logger LOGGER = System.getLogger(LLBService.class.getName());

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public CCStatus getCCStatus() throws IOException {
        final CCStatus result = lLBService.getCCStatus();
        LOGGER.log(System.Logger.Level.DEBUG, "Returning : " + result);
        return result;
    }
}
