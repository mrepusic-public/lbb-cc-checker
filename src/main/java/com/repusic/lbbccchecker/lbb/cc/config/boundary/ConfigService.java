package com.repusic.lbbccchecker.lbb.cc.config.boundary;

import java.lang.System.Logger;
import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import org.apache.commons.lang3.StringUtils;
import org.eclipse.microprofile.config.inject.ConfigProperty;

/**
 * @author Mijo Repušić
 */
@ApplicationScoped
public class ConfigService {
    
    public static final String CONFIG_LBB_USERNAME = "LBBCCCHECKER_LBB_USERNAME";
    @ConfigProperty(name = CONFIG_LBB_USERNAME)
    private String username;
    
    public static final String CONFIG_LBB_PASSWORD = "LBBCCCHECKER_LBB_PASSWORD";
    @ConfigProperty(name = CONFIG_LBB_PASSWORD)
    private String password;

     public static final String CONFIG_LBB_LOGIN_URL = "LBBCCCHECKER_LBB_URL_LOGIN";
    @ConfigProperty(name = CONFIG_LBB_LOGIN_URL)
    private String urlLogin;
    
         public static final String CONFIG_LBB_LOGOUT_URL = "LBBCCCHECKER_LBB_URL_LOGOUT";
    @ConfigProperty(name = CONFIG_LBB_LOGOUT_URL)
    private String urlLogout;
    
    private static final Logger LOGGER = System.getLogger(ConfigService.class.getName());
    
    @PostConstruct
    public void postConstruct(){        
        LOGGER.log(Logger.Level.INFO, CONFIG_LBB_USERNAME + ": " + username);
        LOGGER.log(Logger.Level.INFO, CONFIG_LBB_PASSWORD + ": " + StringUtils.abbreviate(password, 4));
        LOGGER.log(Logger.Level.INFO, CONFIG_LBB_LOGIN_URL + ": " + urlLogin);
        LOGGER.log(Logger.Level.INFO, CONFIG_LBB_LOGOUT_URL + ": " + urlLogout);
    }
    
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUrlLogin() {
        return urlLogin;
    }

    public void setUrlLogin(String urlLogin) {
        this.urlLogin = urlLogin;
    }

    public String getUrlLogout() {
        return urlLogout;
    }

    public void setUrlLogout(String urlLogout) {
        this.urlLogout = urlLogout;
    }
    
    
}
