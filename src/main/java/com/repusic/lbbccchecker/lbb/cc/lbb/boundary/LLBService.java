/*
 */
package com.repusic.lbbccchecker.lbb.cc.lbb.boundary;

import com.repusic.lbbccchecker.lbb.cc.config.boundary.ConfigService;
import com.repusic.lbbccchecker.lbb.cc.lbb.control.CacheController;
import com.repusic.lbbccchecker.lbb.cc.lbb.entity.CCStatus;
import io.github.bonigarcia.wdm.WebDriverManager;
import io.quarkus.cache.CacheResult;
import java.io.File;
import java.io.IOException;
import java.lang.System.Logger.Level;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeDriverService;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 *
 * @author Mijo Repušić
 */
@ApplicationScoped
public class LLBService {

    @Inject
    private ConfigService configService;

    private ChromeDriverService service;

    private WebDriver driver;

    private static final System.Logger LOGGER = System.getLogger(LLBService.class.getName());

    private WebDriverWait wait;

    final By usernameXpath = By.xpath("//input[@name='username']");
    final By passwordXpath = By.xpath("//input[@name='password']");
    final By loginbuttonXpath = By.xpath("//button[contains(text(),'Anmelden')]");

    final By verbrauchtEuroXpath = By.xpath("//div[text()='verbraucht']/following-sibling::div");
    final By verfuegbarEuroXpath = By.xpath("//div[text()='verfügbar']/following-sibling::div");
    final By amazonPunkteXpath = By.xpath("//span[text()=' Amazon Punkte ']/following-sibling::span[1]");

    final By limitEuroXpath = By.xpath("//div[@id='speedometerLimitValue']");

    final By logoutXpath = By.xpath("//a[span/text()='Abmelden']");

    final By userBereichXpath = By.xpath("//div[@id='usermenudropdown']");
    
   
    
    
    @PostConstruct
    public void postConstruct() {
        WebDriverManager.chromedriver().setup();
        this.service = new ChromeDriverService.Builder()
                .usingDriverExecutable(new File(WebDriverManager.chromedriver().getBinaryPath()))
                .usingAnyFreePort()
                .build();
        try {
            service.start();
        } catch (IOException ex) {
            throw new LLBServiceException(ex);
        }
        ChromeOptions options = new ChromeOptions();
        options.addArguments("start-maximized");
        options.addArguments("--headless");
        options.addArguments("enable-automation");
        options.addArguments("--no-sandbox");
        options.addArguments("--disable-infobars");
        options.addArguments("--disable-dev-shm-usage");
        options.addArguments("--disable-browser-side-navigation");
        options.addArguments("--disable-gpu");

        this.driver = new ChromeDriver(service, options);
        this.wait = new WebDriverWait(driver, 30);
    }

     
    @CacheResult(cacheName = CacheController.CACHE_NAME)
    public CCStatus getCCStatus() {
        try {
            login();
            final CCStatus result = new CCStatus();
            result.setVerbrauchtEuro(removeEuro(getVerbrauchtEuro()));
            result.setVerfuegbarEuro(removeEuro(getVerfuegbarEuro()));
            result.setAmazonPunkte(getAmazonPunkte());
            result.setLimitEuro(removeEuro(getLimitEuro()));
            logout();
            return result;
        } catch (IOException ex) {
            throw new LLBServiceException(ex);
        }
    }

   
    private boolean login() throws IOException {
        driver.get(configService.getUrlLogin());
        wait.until(ExpectedConditions.visibilityOfElementLocated(usernameXpath));
        final WebElement usernameInput = driver.findElement(usernameXpath);

        if (LOGGER.isLoggable(Level.DEBUG)) {
            LOGGER.log(System.Logger.Level.DEBUG, driver.getPageSource());
        }
        usernameInput.sendKeys(configService.getUsername());
        final WebElement passwordInput = driver.findElement(passwordXpath);
        passwordInput.sendKeys(configService.getPassword());

        final WebElement loginButton = driver.findElement(loginbuttonXpath);
        loginButton.click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(verbrauchtEuroXpath));
        return true;
    }

    private void logout() {
        driver.navigate().to(configService.getUrlLogout());
    }

    @PreDestroy
    public void destroyed() {
        driver.quit();
        service.stop();
    }

    private String getVerbrauchtEuro() {
        final WebElement logoutButton = driver.findElement(verbrauchtEuroXpath);
        return logoutButton.getText();
    }

    private String getVerfuegbarEuro() {
        final WebElement logoutButton = driver.findElement(verfuegbarEuroXpath);
        return logoutButton.getText();
    }

    private String getAmazonPunkte() {
        final WebElement logoutButton = driver.findElement(amazonPunkteXpath);
        return logoutButton.getText();
    }

    private String getLimitEuro() {
        final WebElement logoutButton = driver.findElement(limitEuroXpath);
        return logoutButton.getText();
    }

    private String removeEuro(String result) {
        return result==null?result:result.replaceAll("€", "").trim();
    }

}
