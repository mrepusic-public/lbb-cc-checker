package com.repusic.lbbccchecker.lbb.cc.lbb.boundary;

import java.io.IOException;

/**
 *
 * @author Mijo Repušić
 */
public class LLBServiceException extends RuntimeException {

    /**
     * Creates a new instance of <code>LLBServiceException</code> without detail
     * message.
     */
    public LLBServiceException() {
    }

    /**
     * Constructs an instance of <code>LLBServiceException</code> with the
     * specified detail message.
     *
     * @param msg the detail message.
     */
    public LLBServiceException(String msg) {
        super(msg);
    }

    LLBServiceException(IOException ex) {
        super(ex);
    }
}
