package com.repusic.lbbccchecker.lbb.cc.lbb.control;

import io.quarkus.cache.CacheInvalidate;
import io.quarkus.scheduler.Scheduled;
import javax.enterprise.context.ApplicationScoped;

/**
 *
 * @author Mijo Repušić
 */
@ApplicationScoped
public class CacheController {
    
     public final static String CACHE_NAME ="ccstatus-cache";
     
     
    private static final System.Logger LOGGER = System.getLogger(CacheController.class.getName());

    
    @CacheInvalidate(cacheName = CACHE_NAME)
    public void invalidateCache(){
        //Nothing - just invalidating
    }
    
    @Scheduled(every = "30s" )
    void invalidateCacheSheduler(){
        invalidateCache();
    }
}
