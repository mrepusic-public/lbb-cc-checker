package com.repusic.lbbccchecker.lbb.cc.lbb.entity;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Mijo Repušić
 */
@XmlRootElement
public class CCStatus implements Serializable{
    
    private String verbrauchtEuro;
    
    private String verfuegbarEuro; 
    
    private String limitEuro;
    
    private String amazonPunkte;

    public String getVerbrauchtEuro() {
        return verbrauchtEuro;
    }

    public void setVerbrauchtEuro(String verbrauchtEuro) {
        this.verbrauchtEuro = verbrauchtEuro;
    }

    public String getVerfuegbarEuro() {
        return verfuegbarEuro;
    }

    public void setVerfuegbarEuro(String verfuegbarEuro) {
        this.verfuegbarEuro = verfuegbarEuro;
    }

    public String getLimitEuro() {
        return limitEuro;
    }

    public void setLimitEuro(String limitEuro) {
        this.limitEuro = limitEuro;
    }

    public String getAmazonPunkte() {
        return amazonPunkte;
    }

    public void setAmazonPunkte(String amazonPunkte) {
        this.amazonPunkte = amazonPunkte;
    }

    @Override
    public String toString() {
        return "CCStatus{" + "verbrauchtEuro=" + verbrauchtEuro + ", verfuegbarEuro=" + verfuegbarEuro + ", limitEuro=" + limitEuro + ", amazonPunkte=" + amazonPunkte + '}';
    }
    
    
}
